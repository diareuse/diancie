package wiki.depasquale.diancie.model

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import wiki.depasquale.diancie.model.internal.ConvertableValue
import wiki.depasquale.diancie.model.internal.CurrencyRate
import kotlin.math.abs
import kotlin.random.Random.Default.nextBytes
import kotlin.random.Random.Default.nextDouble

class TestConversion {

    private lateinit var item: ConvertableValue
    private val positiveDouble get() = TestConversionProvider.positiveDoubleValue
    private val negativeDouble get() = TestConversionProvider.negativeDoubleValue

    @Before
    fun prepare() {
        item = TestConversionProvider.item
    }

    /*
    * Tests whether changes in conversion change the initial value
    * */
    @Test
    fun test_randomConversionInput() {
        val initialConversion = item.conversion
        val inputValue = positiveDouble
        item.value = inputValue
        item.conversion = positiveDouble
        assertThat(item.value).isEqualTo(inputValue / initialConversion * item.conversion)
    }

    /*
    * Tests regression in precision in tolerance of 0.001.
    * This tolerance should be more than enough for displaying in app.
    * */
    @Test
    fun test_selfRepeatedInput() {
        val inputValue = positiveDouble
        item.value = inputValue
        item.value = item.value
        assertThat(item.value).isWithin(0.001).of(inputValue)
    }

    /*
    * Tests input of negative numbers, output of which should be always positive
    * */
    @Test
    fun test_inputNegative() {
        val inputValue = negativeDouble
        item.value = inputValue
        assertThat(item.value).isGreaterThan(0)
    }

    /*
    * Tests negative conversion, output of both conversion and value should always be positive
    * */
    @Test
    fun test_negativeConversion() {
        val conversion = negativeDouble
        item.conversion = conversion
        assertThat(item.value).isGreaterThan(0)
        assertThat(item.conversion).isGreaterThan(0)
    }

}

object TestConversionProvider {

    val positiveDoubleValue get() = abs(nextDouble())
    val negativeDoubleValue get() = -positiveDoubleValue

    val rate get() = CurrencyRate(nextBytes(3).toString(), positiveDoubleValue)
    val item get() = ConvertableValue(rate)

}