package wiki.depasquale.diancie.model.internal

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import wiki.depasquale.diancie.model.R
import java.util.*

@Entity(tableName = "currency")
data class CurrencyRate(
    @PrimaryKey
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "last_value")
    val lastValue: Double
)

// These alone took 2 hours alone to research, resize and rename.
// Poor guy who had to include every. single. one... ugh
val CurrencyRate.flag
    get() = when (code.toLowerCase()) {
        "aud" -> R.drawable.ic_aud
        "bgn" -> R.drawable.ic_bgn
        "brl" -> R.drawable.ic_brl
        "cad" -> R.drawable.ic_cad
        "chf" -> R.drawable.ic_chf
        "cny" -> R.drawable.ic_cny
        "czk" -> R.drawable.ic_czk
        "dkk" -> R.drawable.ic_dkk
        "eur" -> R.drawable.ic_eur
        "gbp" -> R.drawable.ic_gbp
        "hkd" -> R.drawable.ic_hkd
        "hrk" -> R.drawable.ic_hrk
        "huf" -> R.drawable.ic_huf
        "idr" -> R.drawable.ic_idr
        "ils" -> R.drawable.ic_ils
        "inr" -> R.drawable.ic_inr
        "isk" -> R.drawable.ic_isk
        "jpy" -> R.drawable.ic_jpy
        "krw" -> R.drawable.ic_krw
        "mxn" -> R.drawable.ic_mxn
        "myr" -> R.drawable.ic_myr
        "nok" -> R.drawable.ic_nok
        "nzd" -> R.drawable.ic_nzd
        "php" -> R.drawable.ic_php
        "pln" -> R.drawable.ic_pln
        "ron" -> R.drawable.ic_ron
        "rub" -> R.drawable.ic_rub
        "sek" -> R.drawable.ic_sek
        "sgd" -> R.drawable.ic_sgd
        "thb" -> R.drawable.ic_thb
        "try" -> R.drawable.ic_try
        "usd" -> R.drawable.ic_usd
        "zar" -> R.drawable.ic_zar
        else -> R.drawable.ic_unknown
    }

val CurrencyRate.name get() = Currency.getInstance(code).displayName.orEmpty()