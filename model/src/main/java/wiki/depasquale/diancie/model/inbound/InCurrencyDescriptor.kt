package wiki.depasquale.diancie.model.inbound

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InCurrencyDescriptor(
    @Json(name = "base") val base: String,

    //Let's just quietly disregard this field, it only produces unnecessary overhead
    //@Json(name = "date") val date: Long,

    @Json(name = "rates") val rates: Map<String, Double>
)