package wiki.depasquale.diancie.model.internal

import kotlin.math.abs

class ConvertableValue(item: CurrencyRate) {

    var conversion = abs(item.lastValue)
        set(value) {
            field = abs(value)
        }
    var value = 1.0
        get() = field * conversion
        set(value) {
            (abs(value) / conversion)
                .takeUnless { it == field }
                ?.let { field = it }
        }

}