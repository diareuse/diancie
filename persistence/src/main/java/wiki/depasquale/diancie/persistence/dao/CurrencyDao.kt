package wiki.depasquale.diancie.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.skoumal.teanity.persistence.BaseDao
import wiki.depasquale.diancie.model.internal.CurrencyRate

@Dao
interface CurrencyDao : BaseDao<CurrencyRate> {

    @Query("SELECT * FROM currency")
    fun selectAll(): LiveData<List<CurrencyRate>>

}