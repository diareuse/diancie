package wiki.depasquale.diancie.persistence.usecase

import com.skoumal.teanity.persistence.UseCase
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.persistence.dao.CurrencyDao

class CurrencyRateInsertUseCase internal constructor(
    private val dao: CurrencyDao
) : UseCase<List<CurrencyRate>, Unit>() {

    override suspend fun execute(input: List<CurrencyRate>) {
        dao.insert(input)
    }

}