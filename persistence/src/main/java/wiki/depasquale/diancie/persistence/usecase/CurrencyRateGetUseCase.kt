package wiki.depasquale.diancie.persistence.usecase

import com.skoumal.teanity.persistence.LiveUseCase
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.persistence.dao.CurrencyDao

class CurrencyRateGetUseCase internal constructor(
    private val dao: CurrencyDao
) : LiveUseCase<Unit, List<CurrencyRate>>() {

    override fun execute(input: Unit) = dao.selectAll()

}