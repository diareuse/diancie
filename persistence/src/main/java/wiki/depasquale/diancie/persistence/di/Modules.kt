package wiki.depasquale.diancie.persistence.di

import android.content.Context
import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import wiki.depasquale.diancie.persistence.Persistence
import wiki.depasquale.diancie.persistence.usecase.CurrencyRateGetUseCase
import wiki.depasquale.diancie.persistence.usecase.CurrencyRateInsertUseCase

//region Currency
private val currencyModule = module {
    single { get<Persistence>().currency() }
    single { CurrencyRateGetUseCase(get()) }
    single { CurrencyRateInsertUseCase(get()) }
}
//endregion

//region Database
private val databaseModule = module {
    single { createDatabase(androidContext()) }
}

private fun createDatabase(context: Context) =
    Room.databaseBuilder(context, Persistence::class.java, Persistence.name)
        .fallbackToDestructiveMigration()
        .build()
//endregion

val persistenceModules = listOf(
    databaseModule, currencyModule
)
