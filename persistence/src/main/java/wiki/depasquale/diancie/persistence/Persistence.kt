package wiki.depasquale.diancie.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.persistence.dao.CurrencyDao

@Database(
    version = 1,
    entities = [
        CurrencyRate::class
    ]
)
abstract class Persistence : RoomDatabase() {

    companion object {
        const val name = "persistent-data"
    }

    internal abstract fun currency(): CurrencyDao
}
