package wiki.depasquale.diancie.data.di

import org.koin.dsl.module
import wiki.depasquale.diancie.data.usecase.CurrencyRateRefreshUseCase

private val currencyModule = module {
    single { CurrencyRateRefreshUseCase(get(), get()) }
}

val dataModules = listOf(currencyModule)
