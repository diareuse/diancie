package wiki.depasquale.diancie.data.usecase

import com.skoumal.teanity.persistence.UseCase
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.network.RevolutMock
import wiki.depasquale.diancie.persistence.dao.CurrencyDao

class CurrencyRateRefreshUseCase internal constructor(
    private val api: RevolutMock,
    private val dao: CurrencyDao
) : UseCase<Unit, Unit>() {

    private val primaryShorthand = "EUR"

    override suspend fun execute(input: Unit) {
        val rates = api.currency(primaryShorthand).rates
            .map { CurrencyRate(it.key, it.value) } + CurrencyRate(primaryShorthand, 1.0)

        dao.insert(rates)
    }

}