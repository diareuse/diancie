package wiki.depasquale.diancie.data

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.net.toUri
import com.chibatching.kotpref.ContextProvider
import com.chibatching.kotpref.KotprefModel
import org.koin.core.KoinComponent
import org.koin.core.get

object Config : KotprefModel(provider) {

    var nightMode by intPref(AppCompatDelegate.MODE_NIGHT_NO, "night_mode")

}

private val provider = object : ContextProvider, KoinComponent {
    override fun getApplicationContext(): Context = get()
}

// ---

object EditTextClickHelper {

    @JvmStatic
    fun click(view: View) {
        //yeet, fix this :)
        Intent(Intent.ACTION_VIEW)
            .setData("https://drive.google.com/open?id=12xpu204q3sKhs5-uJ7TxBy623QQRDmxu".toUri())
            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .also { view.context.startActivity(it) }
    }

}