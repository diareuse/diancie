package wiki.depasquale.diancie.data

object Constants {

    val isDebug = BuildConfig.DEBUG
    val isRelease = BuildConfig.BUILD_TYPE == "release"

}
