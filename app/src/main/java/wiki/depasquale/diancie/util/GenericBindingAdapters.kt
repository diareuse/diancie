package wiki.depasquale.diancie.util

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.addTextChangedListener
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.bumptech.glide.request.RequestOptions
import com.skoumal.teanity.ui.ScaleType
import com.skoumal.teanity.ui.applyTransformation
import org.koin.core.qualifier.named
import wiki.depasquale.diancie.GlideApp
import wiki.depasquale.diancie.model.internal.CurrencyRate
import java.text.NumberFormat

@BindingAdapter("circularIcon")
fun ImageView.setCircularIcon(resource: Int) {
    GlideApp.with(context)
        .load(resource)
        .apply(RequestOptions().applyTransformation(ScaleType.CIRCLE_CROP))
        .into(this)
}

@BindingAdapter("app:srcCompat")
fun AppCompatImageView.setImage(image: Int) {
    setImageResource(image)
}

@InverseBindingAdapter(attribute = "android:text", event = "onFocusedTextChanged")
fun TextView.getTextAsDouble() = withKoin { get<NumberFormat>(named<CurrencyRate>()) }
    .runCatching { parse(text.toString()).toDouble() }
    .getOrNull() ?: 0.0

@BindingAdapter("android:text")
fun TextView.setTextAsDouble(wannabeText: Double) {
    if (getTextAsDouble() == wannabeText) return
    withKoin { get<NumberFormat>(named<CurrencyRate>()) }
        .format(wannabeText).orEmpty()
        .takeUnless { it == text }
        ?.let { text = it }
}

@BindingAdapter("onFocusedTextChanged")
fun TextView.setFocusedTextChangedListener(listener: InverseBindingListener) {
    addTextChangedListener {
        if (isFocused) {
            listener.onChange()
        }
    }
}

@BindingAdapter("onFocused")
fun EditText.onFocused(listener: () -> Unit) {
    onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
        if (hasFocus) listener()
    }
}

@BindingAdapter("menuItemPressed")
fun Toolbar.setOnMenuItemPressedListener(listener: Toolbar.OnMenuItemClickListener) {
    setOnMenuItemClickListener(listener)
}