package wiki.depasquale.diancie.util

import org.koin.core.Koin
import org.koin.core.context.GlobalContext

inline fun <Result> withKoin(body: Koin.() -> Result) = GlobalContext.get().koin.run(body)