package wiki.depasquale.diancie.ui.converter

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.skoumal.teanity.extensions.hideKeyboard
import com.skoumal.teanity.util.Insets
import org.koin.android.viewmodel.ext.android.viewModel
import wiki.depasquale.diancie.R
import wiki.depasquale.diancie.databinding.FragmentConverterBinding
import wiki.depasquale.diancie.view.AppFragment

class ConverterFragment : AppFragment<ConverterViewModel, FragmentConverterBinding>() {

    override val layoutRes = R.layout.fragment_converter
    override val viewModel by viewModel<ConverterViewModel>()

    override fun consumeSystemWindowInsets(left: Int, top: Int, right: Int, bottom: Int) =
        Insets(left, top, right, bottom)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // FUCK using abstract classes as listeners
        // FUCK non-inline-able abstract classes in general
        binding.currencyContent.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
                    hideKeyboard()
                    activity?.currentFocus?.clearFocus()
                }
            }
        })
    }

}