package wiki.depasquale.diancie.ui

import org.koin.android.viewmodel.ext.android.viewModel
import wiki.depasquale.diancie.R
import wiki.depasquale.diancie.databinding.ActivityHomeBinding
import wiki.depasquale.diancie.view.AppActivity

class HomeActivity : AppActivity<HomeViewModel, ActivityHomeBinding>() {

    override val layoutRes = R.layout.activity_home
    override val viewModel by viewModel<HomeViewModel>()
    override val navHostId = R.id.home_content

}
