package wiki.depasquale.diancie.ui.converter

import android.net.NetworkInfo
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.skoumal.teanity.extensions.bindingOf
import com.skoumal.teanity.extensions.diffListOf
import com.skoumal.teanity.extensions.subscribeK
import com.skoumal.teanity.persistence.invoke
import com.skoumal.teanity.rxbus.RxBus
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.KoinComponent
import org.koin.core.get
import wiki.depasquale.diancie.BR
import wiki.depasquale.diancie.R
import wiki.depasquale.diancie.data.usecase.CurrencyRateRefreshUseCase
import wiki.depasquale.diancie.model.event.RxEvent
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.model.navigation.Actions
import wiki.depasquale.diancie.model.recyclable.CurrencyItem
import wiki.depasquale.diancie.persistence.usecase.CurrencyRateGetUseCase
import wiki.depasquale.diancie.view.AppViewModel
import java.util.concurrent.TimeUnit

@UseExperimental(ExperimentalCoroutinesApi::class)
class ConverterViewModel(
    private val rxBus: RxBus,
    currencies: CurrencyRateGetUseCase,
    private val refresher: CurrencyRateRefreshUseCase
) : AppViewModel(), KoinComponent {

    val items = diffListOf<CurrencyItem>()
    val itemBinding = bindingOf<CurrencyItem> {
        it.bindExtra(BR.viewModel, this)
    }

    private var currentSelection: CurrencyItem? = null
        set(value) {
            value ?: return
            val list = synchronized(items) { items.toMutableList() }
            list.remove(value)
            list.add(0, value)
            launch { items.update(list, items.computeDiff(list)) }
        }

    private val currencyData = currencies()
    private val currencyObserver = Observer<List<CurrencyRate>> {
        when {
            items.size == 0 -> it
                .map { CurrencyItem(it) }
                .also { launch { items.update(it, items.computeDiff(it)) } }
            else -> refreshConversions(it.orEmpty())
        }
    }

    private var interval: Disposable? = null

    init {
        currencyData.observeForever(currencyObserver)

        ReactiveNetwork
            .observeNetworkConnectivity(get())
            .map { it.state() == NetworkInfo.State.CONNECTED }
            .subscribeK {
                when {
                    it -> startInterval()
                    else -> stopInterval()
                }
            }.add()
    }

    override fun onCleared() {
        super.onCleared()

        currencyData.removeObserver(currencyObserver)
    }

    fun onItemFocused(item: CurrencyItem) {
        items.forEach { it.isFocused = it === item }
        currentSelection = item
    }

    fun menuItemPressed(item: MenuItem) = when (item.itemId) {
        R.id.action_change_theme -> Actions.ThemeToggle.publish()
        else -> null
    } != null

    private fun refreshConversions(list: List<CurrencyRate>) {
        list.forEach { rxBus.post(RxEvent.Conversion(it.code, it.lastValue)) }
    }

    private fun startInterval() {
        stopInterval()
        interval = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeK { launch { refresher.now(Unit) } }.also { it.add() }
    }

    private fun stopInterval() {
        interval?.dispose()
    }

}