package wiki.depasquale.diancie

import android.app.Application
import com.skoumal.teanity.di.Teanity
import timber.log.Timber
import wiki.depasquale.diancie.data.di.dataModules
import wiki.depasquale.diancie.di.appModules
import wiki.depasquale.diancie.network.di.networkModules
import wiki.depasquale.diancie.persistence.di.persistenceModules

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        Teanity.startWith(this, persistenceModules + networkModules + dataModules + appModules)
    }

}
