package wiki.depasquale.diancie.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.ViewDataBinding
import com.skoumal.teanity.view.TeanityActivity
import com.skoumal.teanity.viewevent.base.ViewEvent
import wiki.depasquale.diancie.data.Config
import wiki.depasquale.diancie.model.navigation.Actions
import wiki.depasquale.diancie.model.navigation.Destinations

abstract class AppActivity<ViewModel : AppViewModel, Binding : ViewDataBinding> :
    TeanityActivity<ViewModel, Binding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(Config.nightMode)
        super.onCreate(savedInstanceState)
    }

    override fun onEventDispatched(event: ViewEvent) {
        super.onEventDispatched(event)

        when (event) {
            Destinations.Up -> navController.navigateUp()
            Actions.ThemeToggle -> toggleNightMode()
        }
    }

    private fun toggleNightMode() {
        when (Config.nightMode) {
            AppCompatDelegate.MODE_NIGHT_YES -> AppCompatDelegate.MODE_NIGHT_NO
            AppCompatDelegate.MODE_NIGHT_NO -> AppCompatDelegate.MODE_NIGHT_YES
            else -> AppCompatDelegate.MODE_NIGHT_YES
        }.also { Config.nightMode = it }
        recreate()
    }

}
