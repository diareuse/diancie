package wiki.depasquale.diancie.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.skoumal.teanity.view.TeanityFragment
import com.skoumal.teanity.viewevent.base.ViewEvent
import wiki.depasquale.diancie.model.navigation.Actions
import wiki.depasquale.diancie.model.navigation.Destinations

abstract class AppFragment<ViewModel : AppViewModel, Binding : ViewDataBinding> :
    TeanityFragment<ViewModel, Binding>() {

    override fun onEventDispatched(event: ViewEvent) {
        super.onEventDispatched(event)

        when (event) {
            Destinations.Up -> navController.navigateUp()
            Actions.ThemeToggle -> teanityActivity?.onEventDispatched(event)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = super.onCreateView(inflater, container, savedInstanceState).also {
        binding.executePendingBindings()
    }

}
