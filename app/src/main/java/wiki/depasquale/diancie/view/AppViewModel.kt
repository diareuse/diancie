package wiki.depasquale.diancie.view

import com.skoumal.teanity.viewmodel.LoadingViewModel

abstract class AppViewModel : LoadingViewModel()