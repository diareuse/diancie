package wiki.depasquale.diancie.model.recyclable

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import com.skoumal.teanity.databinding.GenericRvItem
import com.skoumal.teanity.extensions.compareToSafe
import com.skoumal.teanity.extensions.subscribeK
import com.skoumal.teanity.rxbus.RxBus
import org.koin.core.KoinComponent
import org.koin.core.inject
import wiki.depasquale.diancie.BR
import wiki.depasquale.diancie.R
import wiki.depasquale.diancie.model.event.RxEvent
import wiki.depasquale.diancie.model.internal.ConvertableValue
import wiki.depasquale.diancie.model.internal.CurrencyRate

abstract class ObservableItem : GenericRvItem(), Observable {

    private val callbacks by lazy { PropertyChangeRegistry() }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            callbacks.remove(callback ?: return)
        }
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        synchronized(this) {
            callbacks.add(callback ?: return)
        }
    }

    protected fun notifyChanged() = callbacks.notifyCallbacks(this, 0, null)
    protected fun notifyPropertyChanged(id: Int) = callbacks.notifyCallbacks(this, id, null)
}

class CurrencyItem(val item: CurrencyRate) : ObservableItem(), KoinComponent {

    override val layoutRes = R.layout.item_currency

    private val rxBus by inject<RxBus>()

    private val convertableValue = ConvertableValue(item)

    private var conversion
        get() = convertableValue.conversion
        set(value) {
            if (!isFocused) {
                // Yeah, I'll make your review easier. This line right here prevents the item from
                // refreshing conversion. This will ultimately make the conversion less accurate
                // however the value will be stable - which is, I think, more important.
                // This could be possibly resolved by using the API ?base=${item.code} and could be
                // improved indefinitely with small tweaks and changes, so I'm not gonna bother with
                // that.
                convertableValue.conversion = value
                notifyValueChanged()
            }
        }
    var value
        @Bindable get() = convertableValue.value
        @Bindable set(value) {
            convertableValue.value = value
            if (isFocused) {
                rxBus.post(RxEvent.Value(item.code, convertableValue.value / conversion))
            }
        }

    var isFocused = false

    init {
        rxBus.register<RxEvent.Conversion> { it.code == item.code }
            .map { it.lastValue }
            .distinctUntilChanged()
            .subscribeK { conversion = it }

        rxBus.register<RxEvent.Value> { it.source != item.code }
            .map { it.input }
            .distinctUntilChanged()
            .subscribeK {
                value = it * conversion
                notifyValueChanged()
            }
    }

    override fun contentSameAs(other: GenericRvItem) = true
    override fun sameAs(other: GenericRvItem) = other.compareToSafe<CurrencyItem> {
        item.code == it.item.code
    }

    private fun notifyValueChanged() = notifyPropertyChanged(BR.value)

}