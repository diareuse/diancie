package wiki.depasquale.diancie.model.navigation

import com.skoumal.teanity.viewevent.base.ViewEvent

object Navigation {

    fun up() = Destinations.Up

}

sealed class Destinations : ViewEvent() {

    object Up : Destinations()

}

sealed class Actions : ViewEvent() {

    object ThemeToggle : Actions()

}
