package wiki.depasquale.diancie.model.event

import com.skoumal.teanity.rxbus.RxBus

sealed class RxEvent : RxBus.Event {

    data class Value(val source: String, val input: Double) : RxEvent()
    data class Conversion(val code: String, val lastValue: Double) : RxEvent()

}