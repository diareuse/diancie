package wiki.depasquale.diancie.di

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.ui.HomeViewModel
import wiki.depasquale.diancie.ui.converter.ConverterViewModel
import java.text.NumberFormat

private val viewModels = module {
    viewModel { HomeViewModel() }
    viewModel { ConverterViewModel(get(), get(), get()) }
}

private val helperModule = module {
    single(named<CurrencyRate>()) {
        NumberFormat.getInstance().also {
            it.maximumFractionDigits = 2
        }
    }
}

// ---

val appModules = listOf(
    viewModels, helperModule
)
