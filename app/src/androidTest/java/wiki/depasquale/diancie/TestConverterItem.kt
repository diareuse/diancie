package wiki.depasquale.diancie

import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.skoumal.teanity.rxbus.RxBus
import org.junit.Before
import org.junit.Test
import org.koin.core.KoinComponent
import org.koin.core.inject
import wiki.depasquale.diancie.model.event.RxEvent
import wiki.depasquale.diancie.model.internal.CurrencyRate
import wiki.depasquale.diancie.model.recyclable.CurrencyItem
import kotlin.math.abs
import kotlin.random.Random

@MediumTest
class TestConverterItem : KoinComponent {

    private lateinit var item: CurrencyItem

    private val positiveDouble get() = TestConverterItemProvider.positiveDoubleValue
    private val negativeDouble get() = TestConverterItemProvider.negativeDoubleValue

    private val rxBus by inject<RxBus>()

    @Before
    fun prepare() {
        item = TestConverterItemProvider.item
        item.isFocused = false
    }

    /*
    * Tests whether input value regresses in precision after user input
    * */
    @Test
    fun test_inputFromUser() {
        val inputValue = positiveDouble
        item.value = inputValue
        assertThat(item.value).isEqualTo(inputValue)
    }

    @Test
    fun test_selfRepeatedInput() {
        val inputValue = positiveDouble
        item.value = inputValue
        item.value = item.value
        assertThat(item.value).isWithin(0.001).of(inputValue)
    }

    @Test
    fun test_inputNegative() {
        val inputValue = negativeDouble
        item.value = inputValue
        assertThat(item.value).isGreaterThan(0)
    }

    @Test
    fun test_updateConversion() {
        val conversion = positiveDouble
        val inputValue = positiveDouble
        item.value = inputValue
        rxBus.post(RxEvent.Conversion(item.item.code, conversion))
        Thread.sleep(100) // too lazy to implement idling, sorry
        assertThat(item.value).isNotEqualTo(inputValue)

    }

    @Test
    fun test_updateConversionNegative() {
        val conversion = negativeDouble
        rxBus.post(RxEvent.Conversion(item.item.code, conversion))
        Thread.sleep(100) // too lazy to implement idling, sorry
        assertThat(item.value).isGreaterThan(0)
    }

    @Test
    fun test_updateValueFromRemote() {
        val conversion = positiveDouble
        val inputValue = positiveDouble
        rxBus.post(RxEvent.Value("unrelated-source", inputValue))
        Thread.sleep(100) // too lazy to implement idling, sorry
        rxBus.post(RxEvent.Conversion(item.item.code, conversion))
        Thread.sleep(100) // too lazy to implement idling, sorry
        assertThat(item.value).isEqualTo(conversion * inputValue)
    }

    @Test
    fun test_updatePreventionOnSelf() {
        val conversion = positiveDouble
        val inputValue = positiveDouble
        rxBus.post(RxEvent.Value(item.item.code, inputValue))
        Thread.sleep(100) // too lazy to implement idling, sorry
        rxBus.post(RxEvent.Conversion(item.item.code, conversion))
        Thread.sleep(100) // too lazy to implement idling, sorry
        assertThat(item.value).isNotEqualTo(conversion * inputValue)
    }

}

object TestConverterItemProvider {

    val positiveDoubleValue get() = abs(Random.nextDouble())
    val negativeDoubleValue get() = -positiveDoubleValue

    val rate get() = CurrencyRate(Random.nextBytes(3).toString(), positiveDoubleValue)
    val item get() = CurrencyItem(rate)

}