package wiki.depasquale.diancie.network.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import wiki.depasquale.diancie.network.BuildConfig
import wiki.depasquale.diancie.network.RevolutMock
import wiki.depasquale.responsesanitizer.ResponseSanitizer

//region Network
private val networkModule = module {
    single { ResponseSanitizer() }
    single { createLogging() }
    single { createClient(get(), get()) }
    single { createMoshi() }

    single<Converter.Factory> { createConverter(get()) }

    single { createRetrofit(get(), get()) }
    single { createService<RevolutMock>(get(), RevolutMock.url) }
}

private val appLoggingLevel
    get() = when (BuildConfig.DEBUG) {
        true -> HttpLoggingInterceptor.Level.BODY
        else -> HttpLoggingInterceptor.Level.NONE
    }

private fun createLogging() = HttpLoggingInterceptor().apply {
    level = appLoggingLevel
}

private fun createClient(
    sanitizer: ResponseSanitizer,
    logging: HttpLoggingInterceptor
) = OkHttpClient.Builder()
    .addInterceptor(sanitizer)
    .addInterceptor(logging)
    .build()

private fun createConverter(moshi: Moshi) = MoshiConverterFactory.create(moshi)

private fun createMoshi() = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private fun createRetrofit(
    okHttpClient: OkHttpClient,
    converterFactory: Converter.Factory
) = Retrofit.Builder()
    .addConverterFactory(converterFactory)
    .client(okHttpClient)

private inline fun <reified T> createService(
    retrofitBuilder: Retrofit.Builder,
    baseUrl: String
) = retrofitBuilder
    .baseUrl(baseUrl)
    .build()
    .create(T::class.java)

//endregion

val networkModules = listOf(networkModule)
