package wiki.depasquale.diancie.network

import retrofit2.http.GET
import retrofit2.http.Query
import wiki.depasquale.diancie.model.inbound.InCurrencyDescriptor

interface RevolutMock {

    companion object {
        internal const val url = "https://revolut.duckdns.org/"
    }

    @GET("latest")
    suspend fun currency(
        @Query("base") base: String = "EUR"
    ): InCurrencyDescriptor

}